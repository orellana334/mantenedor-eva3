<%-- 
    Document   : listaProductos
    Created on : 05-07-2021, 21:50:56
    Author     : AOrellana
--%>


<%@page import="root.dto.Comuna"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    
    List<Comuna> productos = (List<Comuna>)request.getAttribute("listaProductos");
    Iterator<Comuna> itComuna = productos.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Stock Productos</h1>
         <form name="form"   action ="ComunaController" method="POST">

        <table border="1">
            <thead>
            <th>Nombre Producto</th>
            <th>Precio Compra</th>
            <th>Precio Oferta</th>
            <th>Precio Venta</th>
            <th>Stock Disponible</th>
            <th> </th>
        </thead>
        <tbody>
            <%while (itComuna.hasNext()) {
                    Comuna com = itComuna.next();%>
            <tr>
                <td><%= com.getModelo()%></td>
                <td><%= com.getPrecioCompra()%></td>
                <td><%= com.getPrecioOferta()%></td>
                <td><%= com.getPrecioVenta()%></td>
                <td><%= com.getStock()%></td>
                <td><input type="radio" name="seleccion" value="<%=com.getModelo()%>"></td>
            </tr>
            <%}%>
        </tbody>
    </table> 
        <br>
                <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar/Producto</button>
         </form>
    </body>
</html>
